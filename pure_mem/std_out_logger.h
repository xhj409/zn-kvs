// Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.
#ifndef ROCKSDB_PURE_MEM_STD_OUT_LOGGER_H_
#define ROCKSDB_PURE_MEM_STD_OUT_LOGGER_H_

#include "rocksdb/env.h"
#include "util/mutexlock.h"

namespace rocksdb {

class StdOutLogger : public Logger {
 public:
  using Logger::Logv;
  explicit StdOutLogger(InfoLogLevel log_level = InfoLogLevel::INFO_LEVEL):Logger(log_level){};

  void Logv(const char *format, va_list args) override {
    MutexLock _(&mutex_);
    vprintf(format, args);
    printf("\n");
  }

  Status CloseImpl() override {
    closed_ = true;
    return Status::OK();
  }

 private:
  port::Mutex mutex_;
};

extern Status CreateStdOutLoggerFromOptions(const std::string &dbname,
                                            const DBOptions &options,
                                            std::shared_ptr<Logger> *logger);

} // namespace rocksdb

#endif //ROCKSDB_PURE_MEM_STD_OUT_LOGGER_H_
