# 与Rocksdb代码集成

## (26/2/2021)

## 0.实现思路

1. 使用ART算法，替换 memtable 中的 skiplist算法
2. 修改Rocksdb的架构,仅存在一个memtable长驻内存中
3. 在memtable对象内部重新建立内存管理和数据落盘的逻辑

## 1.集成说明

- 与Rocksdb集成的类图说明

![art](images/ART_integration.png)

1. PureMemRep类， 继承 public MemTableRep，新增 memtable的 内存仓库管理类。
2. InlineART类，仿照 inlinskiplist 类，用以实现基于art算法的 内存数据索引。
3. ARTIndex4Mem类，使用适配器模式 将ART算法与 rocksdb 所需的接口进行对接。其中key值使用  userkey。
4. ARTRef类，用于存储 ART算法 key值对应的value。类里面有个双向链表用于存储kv的多个版本数据。
5. ART_ROWEX::Tree类，封装开源 ART算法，提供所需的接口。
6. MvccKey类，用于解析 MVCC 键值。

```C++
struct InlineART<Comparator>::MvccKey {
    Slice userKey_;   // 业务键值
    Slice timestamp_; // cockroachdb生成的HLC时间戳，用于多版本控制
    uint64_t seqNumAndType_; // rocksdb生成的时间戳，用于多版本控制
}
```
